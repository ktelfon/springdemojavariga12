package com.example.demo.ex11;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/pc-games")
public class PCGameController {

//    @Secured("ROLE_USER")
    @GetMapping()
    public String pcGameForm(ModelMap modelMap) {
        modelMap.addAttribute("createMessage", "Create PC Game");
        modelMap.addAttribute("gameForm", new PCGameForm());
        return "pcgame";
    }

//    @Secured("ROLE_ADMIN")
    @GetMapping("/{id}")
    public String getById(@PathVariable("id") Long id) {
        return "pcgame";
    }

    @PostMapping()
    public String pcGameInfo(ModelMap modelMap, PCGameForm pcGameForm) {
        modelMap.addAttribute("pcGameObj", pcGameForm);
        return "pcgame-info";
    }
}
