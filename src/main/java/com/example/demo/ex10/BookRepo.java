package com.example.demo.ex10;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRepo extends JpaRepository<Book, Long> {
    List<Book> findAllByTitle(String title);

    Book findByISBN(String isbn);

    Book findByAuthorAndISBN(String author, String isbn);

    List<Book> findTop3ByAuthorOrderByPagesNumDesc(String author);

    List<Book> findAllByTitleStartingWith(String text);

    List<Book> findAllByPagesNumIsBetween(Integer min, Integer max);

    @Query("SELECT b FROM books b WHERE b.pagesNum >= :amountOfPages")
    List<Book> findWherePagesNumIsGreaterThanX(@Param("amountOfPages")Integer amountOfPages);
}
