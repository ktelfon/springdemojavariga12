package com.example.demo.ex4;

import com.example.demo.ex4.beans.DummyLogger;
import com.example.demo.ex4.beans.ListUtil;
import com.example.demo.ex4.beans.StringUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UtilConfiguration {

    @Bean
    public DummyLogger dummyLogger4(){
        return new DummyLogger();
    }

    @Bean
    public ListUtil listUtility(){
        return new ListUtil();
    }

    @Bean("stringUtility")
    public StringUtil stringUtil(){
        return new StringUtil();
    }
}
