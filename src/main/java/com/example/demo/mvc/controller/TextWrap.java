package com.example.demo.mvc.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@AllArgsConstructor
public class TextWrap {
    @Length(min=2)
    private String text;
}
