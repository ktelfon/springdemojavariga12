package com.example.demo.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller("mvcHelloController")
@SessionAttributes("myInfo")
public class HelloController {

    private List<TextWrap> testData = List.of(new TextWrap("Deniss"), new TextWrap("Car"));

    @GetMapping("/hello")
    public String showHello(final ModelMap modelMap) {
        modelMap.addAttribute(
                "helloMsg",
                "hello in thymeleaf from model map " + modelMap.get("myInfo"));
        modelMap.addAttribute("list", testData);
        modelMap.addAttribute("nameObj", new TextWrap(""));
        return "welcome";
    }

    @PostMapping("/save")
    public String saveSomeText(final ModelMap modelMap, @Valid TextWrap textWrap, Errors errors) {
        modelMap.addAttribute("myInfo", textWrap.getText());
        return "redirect:/hello";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/register")
    public String register() {
        return "register";
    }
}
