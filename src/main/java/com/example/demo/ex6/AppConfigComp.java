package com.example.demo.ex6;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;

@Validated
@Component
@ConfigurationProperties(prefix = "pl.sdacademy.zad6")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppConfigComp {

    @Email
    private String email;
    private String firstName;
    @Size(min=3,max=20)
    private String lastName;
    private String address;
    @Min(18)
    private Integer age;
    @NotEmpty
    private List<String> values;
    @NotEmpty
    private Map<String,String> customAttributes;

    @AssertTrue
    public boolean isAddressValid(){
        return address.split(" ").length == 2;
    }

}
