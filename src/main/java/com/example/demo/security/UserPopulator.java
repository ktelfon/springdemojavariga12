package com.example.demo.security;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

@Service
@RequiredArgsConstructor
public class UserPopulator implements CommandLineRunner {

    private final UserRepo userRepo;
    private final BCryptPasswordEncoder encoder;

    public void run(String... args) throws Exception {
        userRepo.save(User.builder()
                .username("Den")
                .password(encoder.encode("123"))
                .role("ADMIN")
                .build());
        userRepo.save(User.builder()
                .username("Ned")
                .password(encoder.encode("123"))
                .role("USER")
                .build());
    }
}
