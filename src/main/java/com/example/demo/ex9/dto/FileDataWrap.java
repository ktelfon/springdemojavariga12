package com.example.demo.ex9.dto;

import com.example.demo.ex9.model.FileData;
import lombok.Value;

import java.util.List;

@Value
public class FileDataWrap {
    private List<FileData> data;
}
