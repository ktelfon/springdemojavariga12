package com.example.demo.ex9.service;

import com.example.demo.ex9.dto.FileDataWrap;
import com.example.demo.ex9.exception.SdaException;
import com.example.demo.ex9.model.FileData;
import com.example.demo.ex9.repository.FileDataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class FileDataService {

    private final FileDataRepository fileDataRepository;

    public FileDataWrap getAllFileData() {
        return new FileDataWrap(fileDataRepository.findAll());
    }

    public FileData getFileDataById(String id) throws SdaException {
        return fileDataRepository
                .findById(UUID.fromString(id))
                .orElseThrow(()-> new SdaException("No file data with id: " + id));
    }

    public UUID save(FileData fileData) {
        return fileDataRepository.save(fileData).getId();
    }

    public void update(String id, FileData fileData) {
        FileData fromDb = getFileDataById(id);
        fileData.setId(fromDb.getId());
        fileDataRepository.save(fileData);
    }

    public void delete(String id) {
        fileDataRepository.delete(getFileDataById(id));
    }
}
