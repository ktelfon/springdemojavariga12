package com.example.demo.ex9.exception;

public class SdaException extends RuntimeException {
    public SdaException(final String message) {
        super(message);
    }
}
